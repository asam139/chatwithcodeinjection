//
//  AppDelegate.swift
//  ChatWithCodeInjection
//
//  Created by Saul Moreno Abril on 18/01/16.
//  Copyright © 2016 Saul Moreno Abril. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

