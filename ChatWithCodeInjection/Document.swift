//
//  Document.swift
//  ChatWithCodeInjection
//
//  Created by Saul Moreno Abril on 18/01/16.
//  Copyright © 2016 Saul Moreno Abril. All rights reserved.
//

import Cocoa

class Document: NSPersistentDocument {

    override init() {
        super.init()
        // Add your subclass-specific initialization here.
    }

    override func windowControllerDidLoadNib(aController: NSWindowController) {
        super.windowControllerDidLoadNib(aController)
        // Add any code here that needs to be executed once the windowController has loaded the document's window.
    }

    override class func autosavesInPlace() -> Bool {
        return true
    }

    override func makeWindowControllers() {
        // Returns the Storyboard that contains your Document window.
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        if let windowController = storyboard.instantiateControllerWithIdentifier("Document Window Controller") as? NSWindowController{
            self.addWindowController(windowController)
        }
        
        
    }

}
