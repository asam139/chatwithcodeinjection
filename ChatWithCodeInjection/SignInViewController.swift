//
//  ViewController.swift
//  ChatWithCodeInjection
//
//  Created by Saul Moreno Abril on 18/01/16.
//  Copyright © 2016 Saul Moreno Abril. All rights reserved.
//

import Cocoa
import WebKit

class SignInViewController: NSViewController, WebFrameLoadDelegate {
    
    @IBOutlet var webView: WebView!;
    @IBOutlet var inputView: NSView!
    @IBOutlet var inputTextView: NSTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        webView.frameLoadDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url: NSURL? = NSURL(string: "http://localhost/chat/");
        
        if let u = url{
            let urlRequest: NSURLRequest = NSURLRequest(URL: u);
            webView.mainFrame.loadRequest(urlRequest)
        }
        
        
        
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    // TODO: WebFrameLoadDelegate

    func webView(sender: WebView!, didFinishLoadForFrame frame: WebFrame!) {
        let request: NSURLRequest? = frame.dataSource?.request
        if let r = request{
            if r.HTTPMethod == "POST"{
                return;
            }
            
            
            let urlString: NSString? = r.URL?.absoluteString;
            if urlString != nil {
                print(urlString);
            }
        }
        
        
    }

}

